import { userService } from "@/service/user-service";
import { useState, useEffect, useContext } from "react";
import { User } from "../../service/interfaces/user";
import { AuthContext } from "@/auth/auth-context";
import Navigation from "@/src/components/Navigation";
import { Painting } from "../../service/interfaces/painting";
import Winner from "@/src/components/Winner";
import Image from "next/image";
import Register from "@/src/components/Forms/Register";



export default function monCompte() {

    const { token, setToken } = useContext(AuthContext);
    const [user, setUser] = useState<User>()
    const [paintings, SetPaintings] = useState<Painting[]>()

    useEffect(() => {
        const fetchUserData = async () => {
            try {
                const user = await userService.getUserInfos(token);
                setUser(user)
            } catch (error) {
                console.error("Erreur lors de la récupération des informations utilisateur :", error);
            }
        };

        if (token) {
            fetchUserData();
        }
    }, [token]);

    useEffect(() => {
        const fetchUserPainting = async () => {
            try {
                if (user && user.id) {
                    const data = await userService.getUserPage(user.id)
                    SetPaintings(data.paintings);
                }
            } catch (error) {
                console.error("Erreur lors de la récupération des informations utilisateur :", error);
            }
        };

        if (user) {
            fetchUserPainting()
        }
    }, [user]);

    console.log(paintings);
    if (paintings) {
        console.log(paintings[0].picture);
    }


    return (
        <>
            <Navigation />

            <div className="md:flex md:my-20 my-10">
                <div className="w-full mx-5">
                    {paintings && (
                        <Image src={paintings[0].picture} alt="" layout="responsive" width={1000} height={1000} />
                    )}
                </div>

                <div className="w-full mx-5 flex items-center justify-center">
                    {user && (
                        <div className="text-3xl text-white">
                            <p>{user.first_name} {user.nickname} {user.name}</p>
                            <h1>FAIRE LE UPDATE ? ET QUE CE SOIT UN FORMULAIRE</h1>
                        </div>
                    )}
                </div>
            </div>

            <div className="flex flex-col items-center">
                <p className="text-gold text-7xl">Mes tabelos</p>
                <br></br>
                <p className="text-gold text-7xl">v</p>

                <div className="flex">
                    {paintings && paintings.map((painting, index) => (
                        <div key={index} className="w-full">
                            <Image src={painting.picture} alt="" layout="responsive" width={1000} height={1000} />
                            <p>{painting.title}</p>
                        </div>
                    ))}
                </div>


            </div>





        </>
    )
}
