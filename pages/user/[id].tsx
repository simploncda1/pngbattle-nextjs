import { contestService } from "@/service/contest-service";
import { userService } from "@/service/user-service";
import { UserPageView } from "@/src/views/UserPageView";
import { Contest } from "../../service/interfaces/contest";
import { User } from "../../service/interfaces/user";



interface Props {
    actualContest: Contest,
    finalContest: Contest,
    nextContest: Contest,
    user: User,
}

export default function UserId({ actualContest, finalContest, nextContest, user }: Props) {


    return (
        <>
            <UserPageView
                actualContest={actualContest}
                finalContest={finalContest}
                nextContest={nextContest}
                user={user}
            />
        </>
    );
}



export async function getStaticPaths() {

    const allUsers: User[] = await userService.getAllUser();

    const paths = allUsers.map((user) => ({
        params: { id: String(user.id) },
    }));

    return {
        paths,
        fallback: false,
    };
}

export async function getStaticProps({ params }: any) {

    const user: User = await userService.getUserPage(params.id);

    const actualContest: Contest = await contestService.getActual();
    const finalContest: Contest = await contestService.getFinal()
    const nextContest: Contest = await contestService.getNext();


    return {
        props: {
            user,
            actualContest,
            finalContest,
            nextContest,
        },
    };
}