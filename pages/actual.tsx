import { contestService } from "@/service/contest-service";
import { Contest } from "@/service/interfaces/contest";
import { ActualPageView } from "@/src/views/ActualPageView";
import { GetServerSideProps } from "next";

interface Props {
    actualContest: Contest,
    nextContest: Contest,
}


export default function actual({ actualContest, nextContest }: Props) {
    return (
        <>
            <ActualPageView
                actualContest={actualContest}
                nextContest={nextContest}
            />

        </>
    )
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
    return {
        props: {
            actualContest: await contestService.getActual(),
            nextContest: await contestService.getNext(),
        }
    }
}
