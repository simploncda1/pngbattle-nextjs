import { contestService } from "@/service/contest-service";
import { RulesPageView } from "@/src/views/RulesPageView";
import { GetServerSideProps } from "next";
import { Contest } from "../service/interfaces/contest";

interface Props {
    actualContest: Contest,
    finalContest: Contest,
    nextContest: Contest,
    previousContest: Contest,
}



export default function Rules({ actualContest, finalContest, nextContest, previousContest }: Props) {


    return (
        <>
            <RulesPageView
                actualContest={actualContest}
                finalContest={finalContest}
                nextContest={nextContest}
                previousContest={previousContest}
            />
        </>
    )
}


export const getServerSideProps: GetServerSideProps<Props> = async () => {
    return {
        props: {
            actualContest: await contestService.getActual(),
            finalContest: await contestService.getFinal(),
            nextContest: await contestService.getNext(),
            previousContest: await contestService.getPrevious(),
        }
    }
}
