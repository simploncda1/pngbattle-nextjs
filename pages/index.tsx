import { contestService } from '@/service/contest-service'
import { GetServerSideProps } from 'next'
import { Inter } from 'next/font/google'
import { Contest } from '../service/interfaces/contest'
import { HomePageView } from '@/src/views/HomePageView'

const inter = Inter({ subsets: ['latin'] })


interface Props {
  actualContest: Contest,
  finalContest: Contest,
  nextContest: Contest,
  previousContest: Contest,
}

export default function Home({ actualContest, finalContest, nextContest, previousContest }: Props) {

  return (
    <>
      <HomePageView
        actualContest={actualContest}
        finalContest={finalContest}
        nextContest={nextContest}
        previousContest={previousContest}
      />
    </>
  );
}



export const getServerSideProps: GetServerSideProps<Props> = async () => {
  return {
    props: {
      actualContest: await contestService.getActual(),
      finalContest: await contestService.getFinal(),
      nextContest: await contestService.getNext(),
      previousContest: await contestService.getPrevious(),
    }
  }
}
