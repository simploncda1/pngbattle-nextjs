import { AuthContext, AuthContextProvider } from '@/auth/auth-context';
import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import Head from 'next/head';
import { useRouter } from 'next/router';
import { parseCookies } from 'nookies';
import { useContext, useEffect } from 'react';

// import "../auth/axios-config"
// FIXME: Ceci pour que ça marche

const cookies = parseCookies();
const initialToken = cookies.token || null;


export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <AuthContextProvider>
        <Head>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link href="https://fonts.googleapis.com/css2?family=Luxurious+Script&display=swap" rel="stylesheet" />
        </Head>
        <Component {...pageProps} />
      </AuthContextProvider>
    </>
  )
}
