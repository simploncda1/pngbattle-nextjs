import { Painting } from "@/service/interfaces/painting"
import Image from 'next/image'

interface Props {
    painting: Painting,
}

export default function Gallery({ painting }: Props) {
    return (
        <>
            <div
                key={painting.id}
                className='flex  md:mx-5 md:w-[300px] md:h-[300px] my-5 '>
                <Image
                    className="object-cover"
                    // TODO: URL globale 
                    src={process.env.NEXT_PUBLIC_SERVER_URL_PAINTING+painting.picture}
                    alt={painting.picture}
                    height={500}
                    width={500}
                ></Image>
            </div>
            <p className="text-xl">{painting.title}</p>
            <p className="italic">{painting.description}</p>
            <p>{painting.likes && String(painting.likes.number)} ❤️</p>
        </>
    )
}