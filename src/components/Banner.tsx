import { Contest } from "@/service/interfaces/contest";
import getTimeLeft from "@/service/function/getTimeLeft";
import Link from "next/link";

interface Props {
    actualContest: Contest,
    nextContest: Contest,
}

export default function Banner({ actualContest, nextContest }: Props) {


    return (
        <div className='flex flex-col md:flex-row justify-evenly items-center p-5 text-white mt-0 md:mt-10'>

            <div className='flex flex-col items-center text-center justify-evenly h-full md:w-2/4 md:text-xl'>
                <p className='pb-3'>Dessinez avec Passion, Triomphez avec Style,</p>
                <p>Les Concours Mensuels Attendent Vos Œuvres !</p>
            </div>

            <div className='md:flex hidden separator'>
            </div>

            <div className='flex pt-4 md:pt-0 md:w-2/4 items-center flex-col md:text-xl'>
                <p>Le theme du mois est : </p>
                <p className='text-gold w-auto'>{actualContest && actualContest.title}</p>
                <Link
                    className='
                mt-3 mb-3 bg-white p-4 px-10 md:text-3xl text-gold rounded-full
                hover:bg-white hover:bg-opacity-25
            '
                    href={'/participation'}
                >
                    Je participe !
                </Link>
                <p><span>{getTimeLeft(nextContest)}</span> Jours restants</p>
            </div>

        </div>
    )
}