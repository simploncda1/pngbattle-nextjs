import Image from 'next/image'
import { useRouter } from "next/router";
import { Painting } from "@/service/interfaces/painting";
import { paintingService } from "@/service/painting-service";
import { AuthContext } from "@/auth/auth-context";
import { useContext, useEffect, useState } from "react";
import LikeButton from '../LikeButton';
import { getLikedPaiting } from '@/service/function/isPaintingLiked';
import { LikePainting } from '@/service/function/likePainting';

interface Props {
    painting: Painting,
    round_id: number,
    userId: number,
}

export default function FinalistImg({ painting, round_id, userId }: Props) {

    const router = useRouter()
    const { token } = useContext(AuthContext);

    const [error, setError] = useState<string>('');
    const [liked, setLiked] = useState<boolean>(false);

    useEffect(() => {
        getLikedPaiting(painting, round_id, token, setLiked, liked);
    }, [token]);

    const handleLike = LikePainting(token, liked, painting, round_id, setError, setLiked)

    return (
        <>
            <div className="relative">
                <div className='flex flex-col items-center md:mx-5 md:w-[500px] md:h-[500px] my-5'>

                    <Image
                        onClick={() => router.push(`/user/${userId}`)}
                        className='cursor-pointer md:h-full md:w-full object-cover h-64 w-64'
                        src={process.env.NEXT_PUBLIC_SERVER_URL_PAINTING + painting.picture}
                        height={500}
                        width={500}
                        alt={''}
                    />
                    {!error &&
                        <LikeButton liked={liked} handleLike={handleLike} />
                    }
                    {error &&
                        <p className="my-8 text-sm text-red-600">{error}</p>
                    }
                </div>

                <div className="z-10 inset-0 hidden md:flex absolute items-center justify-center pointer-events-none">
                    <Image width={500} height={500} src="/Cadre.png" alt="" />
                </div>

            </div>
        </>
    )
}