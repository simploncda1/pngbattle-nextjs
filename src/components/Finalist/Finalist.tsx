import { Contest } from "@/service/interfaces/contest";
import FinalistImg from "./FinalistImg";
import getTimeLeft from "@/service/function/getTimeLeft";

interface Props {
    nextContest: Contest,
    previousContest: Contest,
}

export default function Finalist({ nextContest, previousContest }: Props) {
    return (
        <>
            <div className='flex flex-col items-center'>
                <p className='luxurious-subtitle'>{previousContest.title}</p>

                <div className='flex flex-col md:flex-row w-full justify-center mt-10 md:mt-20'>

                    {previousContest.rounds[0].paintings.map((painting, index) => (
                        <FinalistImg
                            key={index}
                            userId={painting.user_id}
                            round_id={previousContest.rounds[0].id}
                            painting={painting}
                        />
                    ))}

                </div>
                <p className='text-white md:mt-20 mt-10 md:text-xl'>{getTimeLeft(nextContest)} jours restant</p>
            </div>


        </>
    )
}