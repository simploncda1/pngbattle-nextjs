import Image from "next/image"

interface Props {
    liked: boolean
    handleLike: () => void;
}

export default function LikeButton({ liked, handleLike }: Props) {
    return (
        <>
            <button
                onClick={handleLike}
                className='
                            bg-white p-1 text-center text-gold rounded-full
                            hover:bg-white hover:bg-opacity-25 text-2xl w-2/6 my-5
                            flex items-center justify-center
                            '
            >
                {liked
                    ?
                    <>
                        <p>UNLIKE </p>
                        <Image src={'/heart-full.webp'} alt={`Je n'aime plus`} width={30} height={30} />
                    </>
                    :
                    <>
                        <p>LIKE </p>
                        <Image src={'/heart.webp'} alt={`J'aime`} width={30} height={30} />
                    </>
                }
            </button>
        </>
    )
}