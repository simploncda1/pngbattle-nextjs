import Image from "next/image";
import { Painting } from "@/service/interfaces/painting"
import { useRouter } from "next/router";
import LikeButton from "../LikeButton";
import { useContext, useEffect, useState } from "react";
import { AuthContext } from "@/auth/auth-context";
import { paintingService } from "@/service/painting-service";
import { getLikedPaiting } from "@/service/function/isPaintingLiked";
import { LikePainting } from "@/service/function/likePainting";


interface Props {
    painting: Painting,
    round_id: number,
}

export default function ActualImageDetail({ painting, round_id }: Props) {

    const router = useRouter()
    const { token } = useContext(AuthContext);

    const [error, setError] = useState('');
    const [liked, setLiked] = useState<boolean>(false);

    useEffect(() => {
        getLikedPaiting(painting, round_id, token, setLiked, liked);
    }, [token]);

    const handleLike = LikePainting(token, liked, painting, round_id, setError, setLiked)

    return (
        <>
            <div className="flex flex-col items-center">
                <p>{painting.title} {painting.description}</p>
                <Image
                    onClick={() => router.push(`/user/${painting.user_id}`)}
                    className='cursor-pointer md:h-full md:w-full object-cover w-full'
                    src={process.env.NEXT_PUBLIC_SERVER_URL_PAINTING + painting.picture} height={2000} width={2000} alt={""}
                />
                {!error &&
                    <LikeButton liked={liked} handleLike={handleLike} />
                }
            </div>
            {error &&
                <p className="my-8 text-sm text-red-600">{error}</p>
            }
        </>
    )
}