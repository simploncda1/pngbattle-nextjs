import Link from 'next/link'
import Image from 'next/image'
import { Contest } from '@/service/interfaces/contest'

interface Props {
    actualContest: Contest,
}

export default function ActualImage({ actualContest }: Props) {


    return (
        <Link href={'/actual'} className='relative' > {/* hover */}
            <div className='
                w-auto md:h-[970px] h-60 md:my-64 my-10 bg-cover bg-center bg-no-repeat bg-slate-400 
                flex items-center justify-center 
                bg-[url("/actual.jpg")] 
            '>
                <p className='luxurious-title'>{actualContest.title}</p>
            </div>
            <div className="absolute -z-10 inset-0 flex items-center justify-center">
                <Image width={1300} height={1300} src="/Ellipse.png" alt="" />
            </div>
        </Link>
    )
}