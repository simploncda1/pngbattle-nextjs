import { Contest } from "@/service/interfaces/contest";
import { Painting } from "@/service/interfaces/painting";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ActualImageDetail from "./ActualImageDetail";


interface Props {
    actualContest: Contest,
}


export default function ActualPage({ actualContest }: Props) {

    const router = useRouter();

    const [painting, setPainting] = useState<Painting[]>([]);

    const [remainingParticipation, setRemainingParticipation] = useState<Number>(50);

    useEffect(() => {
        if (actualContest && actualContest.rounds[0]) {
            setPainting(actualContest.rounds[0].paintings);

            const actualParticipation = Number(actualContest.rounds[0].paintings.length)
            const maxParticipation = Number(actualContest.rounds[0].maxParticipation)

            setRemainingParticipation(maxParticipation - actualParticipation)

        }
    }, [actualContest]);

    return (
        <>

            <div className="flex flex-col items-center justify-center">
                <p className="luxurious-title">{String(actualContest.title)}</p>
                <p className="text-white">Nombre de participation restante : {String(remainingParticipation)}</p>

                <div className='w-full flex justify-center md:mb-0 mb-10'>
                    <Link
                        className='
                                md:w-[50%] text-center md:text-6xl
                                md:mt-40 mt-10 bg-white p-4 px-10 text-gold rounded-full
                                hover:bg-white hover:bg-opacity-25
                            '
                        href={'/participation'}>JE PARTICIPE</Link>
                </div>
                <p className="luxurious-title">Toutes les oeuvres</p>
            </div>

            <div className="grid md:grid-cols-3 md:gap-5 text-white">
                {painting && painting.map((paint, index) => (
                        <div key={`${index} + ${paint.id}`} className="m-auto p-5 flex flex-col items-center md:my-1 ">
                            <ActualImageDetail painting={paint} round_id={actualContest.rounds[0].id} />
                        </div>
                ))}
            </div>

            {/* FIXME: voir plus ? */}
            <div className="w-full flex justify-center">
                <button onClick={() => router.back()} className="pt-60 luxurious-title">{`<`}</button>
            </div>
        </>
    )
}