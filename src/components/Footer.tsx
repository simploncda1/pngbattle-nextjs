import { Contest } from '@/service/interfaces/contest'
import getTimeLeft from '@/service/function/getTimeLeft';
import Image from 'next/image'
import Link from 'next/link'


interface Props {
    nextContest: Contest,
}

export default function Footer({ nextContest }: Props) {



    return (
        <>
            <div className='flex md:flex-row flex-col-reverse w-full mt-20 '>
                <div className='w-full'>
                    <Image src={'/FooterImg.png'} alt={''} height={2000} width={2000} />
                </div>
                <div className='w-full flex flex-col justify-center text-white text-lg '>
                    <p className='luxurious-title my-10 md:my-20 md:text-left text-center'>Prochain Theme</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit. . . <Link className='text-blue-600' href={'/'}>Voir Plus ???</Link></p>
                    <p className='my-10'>Commence dans : <span>{getTimeLeft(nextContest)}</span> jours</p>
                    <div className='w-full flex justify-center md:mb-0 mb-10'>
                        <Link
                            className='
                                w-[90%] text-center md:text-6xl
                                mt-3 mb-3 bg-white p-4 px-10 text-gold rounded-full
                                hover:bg-white hover:bg-opacity-25
                            '
                            href={'/participation'}>JE PARTICIPE</Link>
                    </div>
                </div>
            </div>
        </>
    )
}