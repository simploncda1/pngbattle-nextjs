

export default function Separator() {

    return (
        <div className="
        flex flex-col h-[150px] items-center justify-center md:mt-[100px] md:mb-[100px]
        px-0 relative text-gold md:text-9xl text-4xl text-center
        ">
        </div>
    )
}