import { AuthContext } from '@/auth/auth-context';
import { User } from '@/service/interfaces/user';
import { userService } from '@/service/user-service';
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router';
import { destroyCookie, setCookie } from 'nookies';
import React, { useContext, useEffect, useState } from 'react'

export default function Navigation() {
    const { token, setToken } = useContext(AuthContext);
    const router = useRouter();

    const [user, setUser] = useState<User>()

    // TODO: Faire une fonction
    useEffect(() => {
        const fetchUserData = async () => {
            try {
                const user = await userService.getUserInfos(token);
                setUser(user)
            } catch (error) {
                console.error("Erreur lors de la récupération des informations utilisateur :", error);
            }
        };

        if (token) {
            fetchUserData();
        }
    }, [token]);

    return (
        <>
            <header className='flex w-full md:p-6 p-2 pt-5'>
                <div className='flex md:items-end items-center md:flex-row flex-col md:w-1/2 w-full'>
                    <div className='md:w-auto w-1/4'>
                        <Link href={'/'} >
                            <Image
                                src={'/Vector.png'}
                                alt={''}
                                width={0}
                                height={0}
                                sizes="100vw"
                                style={{ width: '100%', height: 'auto' }}
                            />
                        </Link>
                    </div>

                    <nav className='
                    flex md:flex-row items-center pt-5 md:pt-0
                    place-content-evenly w-full text-white md:font-medium font-normal
                    '>
                        <Link className='hover:text-gold hover:text-opacity-75' href={"/"}>PNGBattle</Link>

                        {token
                            ? <Link className='hover:text-gold hover:text-opacity-75' href={"/user/" + user?.id}>Mon compte</Link>
                            : <Link className='hover:text-gold hover:text-opacity-75' href={"/account/login"}>Mon compte</Link>
                        }

                        {token
                            ? <button className='hover:text-gold hover:text-opacity-75' onClick={() => {
                                destroyCookie(null, 'token');
                                window.location.reload();
                            }}>Déconnexion</button>
                            : <Link className='hover:text-gold hover:text-opacity-75' href={"/account/login"}>Connexion</Link>
                        }
                    </nav>
                </div>
            </header>

        </>
    )
}