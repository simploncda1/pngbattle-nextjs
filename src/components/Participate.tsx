import { AuthContext } from "@/auth/auth-context";
import { Painting } from "@/service/interfaces/painting";
import { paintingService } from "@/service/painting-service";
import { useRouter } from "next/router";
import { useContext, useState } from "react";
import { useForm } from "react-hook-form";


export default function Participate() {

    const router = useRouter();

    const { token, setToken } = useContext(AuthContext);

    const [error, setError] = useState('');
    const { register, handleSubmit, formState: { errors } } = useForm<Painting>();

    const [picture, setPicture] = useState<string>('');


    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = () => {
                if (typeof reader.result === 'string') {
                    setPicture(reader.result);
                }
            };
            reader.readAsDataURL(file);
        }
    };

    const onSubmit = async (painting: Painting) => {
        if (!token) {
            setError('Vous devez pour connecter pour participer')
            return;
        }
        try {
            const paintingData = {
                ...painting,
                picture: picture,
            };
            // console.log(paintingData)

            const data = await paintingService.postPainting(paintingData, token);
            console.log(data);
            
            // router.push("/actual")
        } catch (error: any) {
            if (error.response?.status == 409) {
                setError('Vous avez déjà participé pour ce Contest !');
            } else {
                setError('Server error');
            }
            console.log(error);
        }
    };

    return (
        <>
            <div className="w-full flex justify-center items-center md:pt-0 pt-20 md:h-[80vh]">
                <form className="flex flex-col md:w-1/5 space-y-2" onSubmit={handleSubmit(onSubmit)}>

                    <label className="text-gold text-xl" htmlFor="title">Titre :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="text"
                        placeholder="title"
                        defaultValue="Amanda"
                        required
                        {...register("title")}
                    />

                    <label className="text-gold text-xl" htmlFor="description">Description :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="text"
                        placeholder="description"
                        defaultValue="tkt c pas une photo"
                        required
                        {...register("description")}
                    />

                    <label className="text-gold text-xl" htmlFor="picture">Url du dessin :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="file"
                        placeholder="picture"
                        required
                        onChange={handleFileChange}
                    // {...register("picture")}
                    />

                    {/* Affichez l'aperçu de l'image si nécessaire */}
                    {picture && <img src={picture} alt="Preview" style={{ maxWidth: '100%', maxHeight: '200px' }} />}

                    {/* // defaultValue="https://cdna.artstation.com/p/assets/images/images/039/280/926/large/alexandre-hildans-portrait-un-peu-plus-portrait-donc-sans-boobs.jpg?1625475634" */}


                    {error &&
                        <p className="mt-2 text-sm text-red-600">{error}</p>
                    }

                    <div className="flex justify-center">
                        <button
                            className="
                        bg-white p-1 text-center text-gold rounded-full
                        hover:bg-white hover:bg-opacity-25 text-xl w-auto px-3 py-1 mt-5
                        "
                            type="submit"
                        >Participer
                        </button>
                    </div>
                </form>
            </div>
        </>
    )
}