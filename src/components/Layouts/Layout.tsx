import Banner from "../Banner";
import Footer from "../Footer";
import Navigation from "../Navigation";


interface Props {
  actualContest: any,
  nextContest: any,
}

const Layout = ({ children, actualContest, nextContest }: Props & { children: React.ReactNode }) => {


  return (
    <>
      <Navigation />
      <Banner
        actualContest={actualContest}
        nextContest={nextContest}
      />
      <main>{children}</main>
      <Footer nextContest={nextContest} />
      {/* Autres éléments de mise en page */}
    </>
  );
};

export default Layout;
