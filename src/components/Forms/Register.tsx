import { AuthContext } from "@/auth/auth-context";
import { authService } from "@/auth/auth-service";
import { User } from "@/service/interfaces/user";
import { useRouter } from "next/router";
import { useContext, useState } from "react";
import { useForm } from "react-hook-form";


export default function register() {

    const router = useRouter();

    const { token, setToken } = useContext(AuthContext);

    const [error, setError] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    const { register, handleSubmit, formState: { errors } } = useForm<User>();

    const onSubmit = async (user: User) => {
        try {
            await authService.postRegister(user);
            setSuccessMessage('Compte créé avec succès !');

            setTimeout(async () => {
                try {
                    const token = await authService.postLogin(String(user.email), String(user.password));
                    const tokenString: string = token.token;
                    await setToken(tokenString);
                    router.push('/');
                } catch (error: any) {
                    if (error.response?.status == 401) {
                        setError('Identifiant / Mot de Passe incorrect');
                    } else {
                        setError('Server error');
                    }
                    console.log(error);
                }
            }, 1000);

        } catch (error: any) {
            if (error.response?.status == 409) {
                setError('Utilisateur déjà existant');
            } else {
                setError('Server error');
            }
            console.log(error);
        }
    };

    return (
        <>
            {successMessage &&
                <div className="loadingMessage text-green-500 text-xl">
                    <p>{successMessage}</p>
                    <p>Connexion en cours</p>
                    <div className="loadingDots"></div>
                </div>
            }

            {!successMessage &&
                <form className="flex flex-col md:w-1/3 space-y-2" onSubmit={handleSubmit(onSubmit)}>

                    <label className="text-gold text-xl" htmlFor="name">Name :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="text"
                        placeholder="name"
                        defaultValue="test name"
                        required
                        {...register("name")}
                    />

                    <label className="text-gold text-xl" htmlFor="first_name">first_name :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="text"
                        placeholder="first_name"
                        defaultValue="test first_name"
                        required
                        {...register("first_name")}
                    />

                    <label className="text-gold text-xl" htmlFor="nickname">nickname :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="text"
                        placeholder="nickname"
                        defaultValue="test nickname"
                        required
                        {...register("nickname")}
                    />

                    <label className="text-gold text-xl" htmlFor="email">Mail :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="email"
                        placeholder="Mail"
                        defaultValue="test-front@test.com"
                        required
                        {...register("email")}
                    />

                    <label className="text-gold text-xl" htmlFor="birthdate">birthdate :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="date"
                        placeholder="birthdate"
                        defaultValue="2000-01-01"
                        required
                        {...register("birthdate")}
                    />

                    <label className="text-gold text-xl" htmlFor="gender">gender :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="text"
                        placeholder="gender"
                        defaultValue="test gender"
                        required
                        {...register("gender")}
                    />

                    <label className="text-gold text-xl" htmlFor="password">Mot de Passe :</label>
                    <input
                        className="text-lg rounded-lg block w-full p-2.5"
                        type="password"
                        placeholder="Mot de Passe"
                        defaultValue="test"
                        required
                        {...register("password")}
                    />

                    {error &&
                        <p className="mt-2 text-sm text-red-600">{error}</p>
                    }

                    <div className="flex justify-center">
                        <button
                            className="
                        bg-white p-1 text-center text-gold rounded-full
                        hover:bg-white hover:bg-opacity-25 text-xl w-auto px-3 py-1 mt-5
                        "
                            type="submit"
                        >S'inscrire
                        </button>
                    </div>
                </form>
            }
        </>
    )
}