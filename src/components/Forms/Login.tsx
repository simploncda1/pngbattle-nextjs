import { useRouter } from "next/router";
import { useContext, useState } from "react";
import { User } from "@/service/interfaces/user";
import { AuthContext } from "@/auth/auth-context";
import { useForm } from "react-hook-form";
import { authService } from "@/auth/auth-service";
import Link from "next/link";



export default function login() {

    const router = useRouter();

    const { token, setToken } = useContext(AuthContext);

    const [error, setError] = useState('');
    const { register, handleSubmit, formState: { errors } } = useForm<User>();

    const onSubmit = async (data: User) => {
        try {
            const token = await authService.postLogin(String(data.email), String(data.password));
            const tokenString: string = token.token;
            await setToken(tokenString);
            router.back();
        } catch (error: any) {
            if (error.response?.status == 401) {
                setError('Identifiant / Mot de Passe incorrect');
            } else {
                setError('Server error');
            }
            console.log(error);
        }
    };

    return (
        <>
            <form className="flex flex-col md:w-1/5 space-y-2" onSubmit={handleSubmit(onSubmit)}>

                <label className="text-gold text-xl" htmlFor="email">Mail :</label>
                <input
                    className="text-lg rounded-lg block w-full p-2.5"
                    type="email"
                    placeholder="Mail"
                    defaultValue="AlexandreHildans@gmail.com"
                    required
                    {...register("email")}
                />

                <label className="text-gold text-xl" htmlFor="password">Mot de Passe :</label>
                <input
                    className="text-lg rounded-lg block w-full p-2.5"
                    type="password"
                    placeholder="Mot de Passe"
                    defaultValue="test"
                    required
                    {...register("password")}
                />

                {error &&
                    <p className="mt-2 text-sm text-red-600">{error}</p>
                }

                <p className="text-white">Pas de compte ?
                    <Link
                        href={"/account/register"}
                        className="text-blue px-1 text-lg underline hover:text-blue hover:text-opacity-75"
                    >S'inscrire</Link>
                </p>

                <div className="flex justify-center">
                    <button
                        className="
                        bg-white p-1 text-center text-gold rounded-full
                        hover:bg-white hover:bg-opacity-25 text-xl w-auto px-3 py-1 mt-5
                        "
                        type="submit"
                    >Connexion
                    </button>
                </div>

            </form>
        </>
    )
}