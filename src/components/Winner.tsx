import { Contest } from "@/service/interfaces/contest";
import { Painting } from "@/service/interfaces/painting";
import { User } from "@/service/interfaces/user";
import Image from "next/image";
import Link from "next/link";

interface Props {
    finalContest: Contest,
    hideTheme?: boolean,
    lastPainting?: Painting[],
    userProfil?: User,
}

export default function Winner({ finalContest, hideTheme, lastPainting, userProfil }: Props) {

    const image = finalContest.rounds[0].paintings[0].picture
    const user = finalContest.users[0]

    const [yearStr, monthStr] = finalContest.length.split('-');
    const year = parseInt(yearStr);
    const month = parseInt(monthStr);
    const date = [month, "/", year]

    return (
        <>
            <div className="relative">

                <div className='flex flex-col items-center' style={{ color: 'white' }}>

                    <div className='flex justify-center flex-col md:flex-row relative'>


                        <>
                            {hideTheme && lastPainting && lastPainting.length >= 1 &&
                                <div className="md:absolute -left-16">
                                    <p className='md:-rotate-90 md:h-0 mt-5 md:mt-10 md:font-bold size '>{date}</p>
                                </div>
                            }

                            {!hideTheme &&
                                <div className="md:absolute -left-16">
                                    <p className='md:-rotate-90 md:h-0 mt-5 md:mt-10 md:font-bold size '>{date}</p>
                                </div>
                            }

                            <div className='md:w-auto'>
                                <Link href={userProfil ? `/user/${userProfil.id}` : `/user/${user.id}`} className="relative">

                                    {hideTheme && lastPainting && lastPainting.length >= 1 &&
                                        <Image
                                            className='object-cover'
                                            src={process.env.NEXT_PUBLIC_SERVER_URL_PAINTING + lastPainting[0].picture}
                                            width={800}
                                            height={800}
                                            alt=''
                                        />
                                    }

                                    {!hideTheme &&
                                        < Image
                                            className='object-cover'
                                            src={process.env.NEXT_PUBLIC_SERVER_URL_PAINTING + image}
                                            width={800}
                                            height={800}
                                            alt=''
                                        />
                                    }
                                </Link>
                            </div>
                        </>

                    </div>

                    <p className='luxurious-title'>
                        {userProfil
                            ? `${userProfil.first_name}  ${userProfil.nickname} ${userProfil.name}`
                            : `${user.first_name} ${user.nickname} ${user.name}`
                        }
                    </p>

                    {!hideTheme &&
                        <p className='luxurious-subtitle'>Pour le theme : <span>{finalContest.title}</span></p>
                    }

                </div>

                {lastPainting && lastPainting.length > 1 &&
                    <>
                        <div className="absolute -mt-40 -z-10 inset-0 hidden md:flex items-center justify-center pointer-events-none">
                            <Image width={1200} height={1200} src="/Polygon.png" alt="" />
                        </div>

                        <div className="absolute -mt-40 inset-0 hidden md:flex items-center justify-center pointer-events-none">
                            <Image width={1200} height={1200} src="/Polygon2.png" alt="" />
                        </div>
                    </>
                }
            </div>

        </>
    )
}
