import Navigation from "../components/Navigation";
import Participate from "../components/Participate";



export function ParticipationPageView() {
    return (
        <>
            <Navigation />

            <Participate />
        </>
    )
}