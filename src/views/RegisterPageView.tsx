import Register from "../components/Forms/Register";
import Navigation from "../components/Navigation";



export function RegisterPageView(){
    return(
        <>
            <Navigation />

            <div className="flex justify-center items-center md:h-[80vh] h-full py-10">
                <Register />
            </div>
        </>
    )
}