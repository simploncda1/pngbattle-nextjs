import Login from "../components/Forms/Login";
import Navigation from "../components/Navigation";



export function LoginPageView() {
    return (
        <>
            <Navigation />

            <div className="flex justify-center items-center md:h-[80vh] h-[70vh]">
                <Login />
            </div>
        </>
    )
}