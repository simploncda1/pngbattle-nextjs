import { Contest } from '@/service/interfaces/contest'
import { User } from '@/service/interfaces/user'
import Layout from '../components/Layouts/Layout'
import Winner from '../components/Winner'
import Gallery from '../components/Profil/Gallery'


interface Props {
    actualContest: Contest,
    finalContest: Contest,
    nextContest: Contest,
    user: User,
}


export function UserPageView({ actualContest, finalContest, nextContest, user }: Props) {


    return (
        <>
            <Layout
                actualContest={actualContest}
                nextContest={nextContest}
            >

                <Winner
                    finalContest={finalContest}
                    lastPainting={user.paintings}
                    userProfil={user}
                    hideTheme={true}
                />

                <div className="w-full flex flex-col items-center mt-20 md:mt-60">
                    <p className="luxurious-title">Mes Oeuvres</p>
                    {user.paintings && user.paintings.length == 0 &&
                        <>
                            <div className='text-white text-xl flex  flex-col items-center'>
                                <p>Pas de dessins postés !</p>
                                <p>Je participe !</p>
                            </div>
                        </>
                    }

                    <div className="flex w-full flex-col md:flex-row justify-center">
                        {user.paintings.map((painting, index) => (
                            <>
                                <div className="flex flex-col items-center text-white md:my-20">
                                    <Gallery key={index} painting={painting} />
                                </div>
                            </>
                        ))}
                    </div>
                </div>


            </Layout>
        </>
    )
}
