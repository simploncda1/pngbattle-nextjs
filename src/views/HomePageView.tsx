import { Contest } from "@/service/interfaces/contest";
import Actual from "../components/Actual/ActualImage";
import Finalist from "../components/Finalist/Finalist";
import Layout from "../components/Layouts/Layout";
import Separator from "../components/Separator";
import Winner from "../components/Winner";


interface Props {
  actualContest: Contest,
  finalContest: Contest,
  nextContest: Contest,
  previousContest: Contest,
}


export function HomePageView({ actualContest, finalContest, nextContest, previousContest }: Props) {



  return (
    <>
      <Layout
        actualContest={actualContest}
        nextContest={nextContest}
      >

        <Winner
          finalContest={finalContest}
        />

        <Separator />

        <div className='flex justify-end'>
          <p className='luxurious-title w-full mb-10 md:mb-40 md:text-right text-center'>Votez pour le prochain !</p>
        </div>

        <Finalist
          nextContest={nextContest}
          previousContest={previousContest}
        />

        <Separator />

        <Actual actualContest={actualContest} />

      </Layout>
    </>
  )
}
