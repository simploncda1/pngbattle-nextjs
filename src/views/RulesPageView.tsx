import { Contest } from "@/service/interfaces/contest";
import Actual from "../components/Actual/ActualImage";
import Finalist from "../components/Finalist/Finalist";
import Layout from "../components/Layouts/Layout";
import Separator from "../components/Separator";
import Winner from "../components/Winner";
import Navigation from "../components/Navigation";
import Banner from "../components/Banner";
import image from "next/image";
import Link from "next/link";
import Image from "next/image"

interface Props {
  actualContest: Contest,
  finalContest: Contest,
  nextContest: Contest,
  previousContest: Contest,
}


export function RulesPageView({ actualContest, finalContest, nextContest, previousContest }: Props) {

  return (
    <>
      <Navigation />
      <Banner
        actualContest={actualContest}
        nextContest={nextContest}
      />

      <div className="flex flex-col items-center">

        <p className="text-gold text-9xl">Regles</p>

        <div className="flex mt-8">

          <div className='w-1/2 flex justify-center flex-col md:flex-row'>

            <div className="md:absolute -left-16">
              <p className='md:-rotate-90 md:h-0 mt-5 md:mt-10 md:font-bold size '>{finalContest.length}</p>
            </div>

            <div className='md:w-auto'>
              <Link href={`/user/${finalContest.users[0].id}`}>
                <Image
                  className='object-cover'
                  src={finalContest.rounds[0].paintings[0].picture}
                  width={1600}
                  height={1600}
                  alt=''
                />
              </Link>
            </div>

          </div>



          <div className="w-1/2 text-white text-5xl bg-cyan-600">
            <ul>
              <li>- png</li>
              <li>- carré</li>
              <li>- no furry</li>
              <li>- blablabla</li>
              <li>- oui je sais pas</li>
            </ul>
          </div>
        </div>


      </div>

      {/* <Winner
        finalContest={finalContest}
      /> */}


    </>
  )
}
