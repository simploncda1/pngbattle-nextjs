import { Contest } from "@/service/interfaces/contest";
import Layout from "../components/Layouts/Layout";
import ActualPage from "../components/Actual/ActualPage";
import Banner from "../components/Banner";
import Navigation from "../components/Navigation";


interface Props {
    actualContest: Contest,
    nextContest: Contest,
}

export function ActualPageView({ actualContest, nextContest }: Props) {
    return (
        <>
            <Navigation />
            <Banner actualContest={actualContest} nextContest={nextContest} />
            <ActualPage actualContest={actualContest} />
        </>
    )
}