import { User } from "@/service/interfaces/user";
import axios from "axios";

export const authService = {

    async postRegister(user: User) {
        const response = await axios.post<{ user: User, token: string }>(process.env.NEXT_PUBLIC_SERVER_URL + '/api/register', user);
        return response.data;
    },

    async postLogin(email: string, password: string) {
        const response = await axios.post<{ token: string }>(process.env.NEXT_PUBLIC_SERVER_URL + '/api/login', { email, password });
        return response.data;
    },
}