import { User } from "@/service/interfaces/user";
import axios from "axios";



export const userService = {

    async getAllUser() {
        const response = await axios.get<any[]>(process.env.NEXT_PUBLIC_SERVER_URL + '/api/user');
        return response.data
    },

    async getUserPage(id: number) {
        const response = await axios.get<any>(process.env.NEXT_PUBLIC_SERVER_URL + '/api/user/' + id);
        return response.data
    },

    async getUserInfos(token: string | null) {
        const response = await axios.get<User>('http://localhost:8080/api/account'
            ,
            {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        return response.data
    }

}

