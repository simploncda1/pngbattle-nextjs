import { Painting } from "../interfaces/painting";
import { paintingService } from "../painting-service";

/**
 * Retrieves the liked status of a painting and updates the state accordingly.
 * 
 * @param painting The painting object to check the liked status for.
 * @param token The authentication token for accessing the service.
 * @param setLiked React state setter function to update the liked status.
 * @param liked Current liked status of the painting.
 */
export function getLikedPaiting(painting: Painting, round_id: number, token: string | null, setLiked: React.Dispatch<React.SetStateAction<boolean>>, liked: boolean) {
    const fetchData = async () => {
        if (painting.id && token) {
            const data = await paintingService.getLikedPainting(painting.id, round_id, token);
            if (data) {
                setLiked(!liked);
            }
        }
    };
    fetchData();
}
