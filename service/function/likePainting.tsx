import { Painting } from "../interfaces/painting";
import { paintingService } from "../painting-service";


/**
 * Handles the process of liking a painting.
 * 
 * @param token The authentication token for accessing the service.
 * @param liked Current liked status of the painting.
 * @param painting The painting object to be liked.
 * @param setError React state setter function to update error messages.
 * @param setLiked React state setter function to update the liked status.
 * @returns A function that, when called, initiates the process of liking the painting.
 */
export function LikePainting(
    token: string | null,
    liked: boolean,
    painting: Painting,
    round_id: number,
    setError: React.Dispatch<React.SetStateAction<string>>,
    setLiked: React.Dispatch<React.SetStateAction<boolean>>,
) {
    return async () => {
        if (token == null) {
            setError('Vous devez vous connecter pour liker');
            return;
        }
        try {
            if (painting && painting.id) {
                const data = await paintingService.likePainting(painting.id, round_id, token);
                setLiked(!liked);
                setError("")
                return data;
            }
        } catch (error: any) {
            if (error.response && error.response.status === 403) {
                setError('Vous avez déjà liké un tableau du thème en cours');
            } else {
                setError('Server error');
            }
        }
    };
}
