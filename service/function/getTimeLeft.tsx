import { Contest } from "@/service/interfaces/contest";
import { useState, useEffect } from "react";


/**
 * Calculates the time left until the start of a contest.
 * 
 * @param contest The contest object containing information about the contest start date.
 * @returns The number of days left until the contest starts.
 */
export default function getTimeLeft(contest: Contest) {
    const [timeLeft, setTimeLeft] = useState<number>();

    useEffect(() => {
        if (contest && contest.length) {
            const contestDateStr = contest.length;
            const [yearStr, monthStr, dayStr] = contestDateStr.split('-');
            const year = parseInt(yearStr);
            const month = parseInt(monthStr);
            const day = parseInt(dayStr);

            const contestDate = new Date(year, month - 1, day);
            const interval: number = contestDate.getTime() - new Date().getTime();
            const dayLeft: number = Math.floor(interval / (1000 * 60 * 60 * 24));
            setTimeLeft(dayLeft);
        }
    }, []);

    return timeLeft;
}