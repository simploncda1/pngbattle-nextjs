import axios from "axios";
import { Painting } from "./interfaces/painting";


export const paintingService = {

    async postPainting(painting: Painting, token: string | null) {
        const response = await axios.post<Painting>(process.env.NEXT_PUBLIC_SERVER_URL + '/api/painting/round', painting,
            {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        return response.data
    },

    async likePainting(paint_id: number, round_id: number, token: string | null) {
        const response = await axios.post(process.env.NEXT_PUBLIC_SERVER_URL + '/api/likes/' + paint_id + '/like/' + round_id,
            {}, // FIXME: Nécéssaire pas sur de comprendre pourquoi
            {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        return response.data;
    },

    async getLikedPainting(paint_id: number, round_id: number, token: string | null) {
        const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/likes/' + paint_id + '/painting/' + round_id,
            {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        );
        return response.data;
    },
}