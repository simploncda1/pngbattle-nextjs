import { Contest } from "@/service/interfaces/contest";
import axios from "axios";


export const contestService = {

    async getActual() {
        const response = await axios.get<Contest>(process.env.NEXT_PUBLIC_SERVER_URL + '/api/contest/actual');
        return response.data
    },

    async getFinal() {
        const response = await axios.get<Contest>(process.env.NEXT_PUBLIC_SERVER_URL + '/api/contest/final');
        return response.data
    },

    async getNext() {
        const response = await axios.get<Contest>(process.env.NEXT_PUBLIC_SERVER_URL + '/api/contest/next');
        return response.data
    },

    async getPrevious() {
        const response = await axios.get<Contest>(process.env.NEXT_PUBLIC_SERVER_URL + '/api/contest/previous');
        return response.data
    },
}