import { Painting } from "./painting";


export interface Round {
    id: number;
    phase: string;
    maxParticipation: number;
    paintings: Painting[];
}