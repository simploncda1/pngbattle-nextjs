import { Painting } from "./painting";


export interface User {
    id?: number;
    name: string;
    first_name: string;
    nickname: string;
    email: null | string;
    birthdate: null | string;
    gender: null | string;
    password: null | string;
    paintings: Painting[];
}