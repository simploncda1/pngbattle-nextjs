import { Likes } from "./likes";


export interface Painting {
    id?: number;
    title: string;
    description: string;
    picture: string;
    date: [number, number, number];
    user_id: number;
    likes?: Likes;
  }