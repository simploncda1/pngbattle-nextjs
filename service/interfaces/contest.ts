import { Round } from "./round";
import { User } from "./user";


export interface Contest {
    id: number;
    length: string;
    title: string,
    rounds: Round[];
    users: User[];
}